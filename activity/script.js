let trainer = {
	name: "Ash Ketchum",
	age: 10,
	health: 100,
	pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	friends: {
		hoen: ["May","Max"],
		kanto: ["Brock","Misty"]
	},
	talk: function(){
		console.log(`Pikachu I choose you!`)
			}
}
console.log(trainer)
console.log(trainer.name)
console.log(trainer["pokemon"])
console.log(trainer.talk())

let myPokemon = {
	name: "Pikachu",
	level: 12,
	health: 24,
	attack: 12,
	tackle: function(){
		console.log(`ThisPokemon tackled targetPokemon`)
		console.log(`targetPokemon's health is now reduced to targetPokemonhealth`)
	},
	faint: function(){
		console.log(`targetPokemon has fainted`)
	}
}

function Pokemon(name,lvl,hp){
	this.name = name,
	this.level = lvl,
	this.health = hp,
	this.attack = lvl,
	this.tackle = function(target){

		console.log(`${this.name} tackled ${target.name}`)
		console.log(`${target.name}'s health is now reduced to ${target.health - this.attack}`)

		target.health -= this.attack

		if(target.health <= 0){
			target.faint(target)
		}

		console.log(target)

	},
		this.faint = function(target){
		console.log(`${target.name} has fainted`)
	}		
}

let pikachu = new Pokemon("Pikachu", 12, 24)
let geodude = new Pokemon("Geodude", 8, 16)
let mewtwo = new Pokemon("Mewtwo", 100, 200)

console.log(pikachu)
console.log(geodude)
console.log(mewtwo)
console.log(geodude.tackle(pikachu))
console.log(mewtwo.tackle(geodude))




